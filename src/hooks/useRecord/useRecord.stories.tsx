import { Button, Stack, TextField } from "@mui/material";
import * as React from "react";
import { useRecord } from "./useRecord";

enum Gender {
  F = "female",
  M = "male",
  O = "other",
}

type User = {
  firstName: string;
  lastName: string;
  email: string;
  age: number;
  gender: Gender;
};

export default {
  title: "Hooks",
};

export const Record = () => {
  const { values, bindEntry } = useRecord<User>();

  const handleSubmit = React.useCallback(() => {
    console.log(values);
  }, []);

  return (
    <Stack direction="column" gap={2}>
      <TextField label="First Name" {...bindEntry("firstName")} />
      <TextField label="Last Name" {...bindEntry("lastName")} />
      <TextField label="Email" {...bindEntry("email")} />

      <pre>{JSON.stringify(values, null, 2)}</pre>
    </Stack>
  );
};
