import { useCallback, useState } from "react";
import { Maybe } from "../../utils/types";

/**
 * This is the combination of any change event types we might encounter
 * with default inputs and others like MUI's SelectChangeEvent
 */
type ChangeEvent<T = string> =
  | (Event & { target: { value: T; name: string } })
  | React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>;

/**
 * We combine the key (K) and value (V) types so we can reference each
 * specific { [K]: V } in T
 */
type RegisterReturn<K, V> = {
  name: K;
  value: Maybe<V>;
  onChange: (val: Maybe<V> | ChangeEvent<V>) => void;
};

/**
 * React's Synthetic Events are not instances of Event so we need to make
 * this type guard to check if onChange took in an event or the actual value.
 *
 * Also, checking val is ChangeEvent<T> also doesn't rule out T itself
 * can be a ChangeEvent so we are checking the other way around. If
 * there's no target value, then it's not a ChangeEvent = is T
 */
const notEvent = <T>(val: ChangeEvent<T> | Maybe<T>): val is T =>
  (val as ChangeEvent<T>)?.target?.value === undefined;

export const useRecord = <T extends { [K in keyof T]: T[K] }>(
  initialValues: T = <T>{}
) => {
  const [values, setValues] = useState(initialValues);

  return {
    values,
    /**
     * partial or complete update of record
     */
    update: useCallback((data: Partial<T>) => {
      setValues((prev) => ({
        ...prev,
        ...data,
      }));
    }, []),
    /**
     * get specific entry value and updater
     */
    bindEntry: useCallback(
      // For each key (K) we get the corresponding type in T[K]
      <K extends keyof T>(key: K): RegisterReturn<K, T[K]> => ({
        name: key,
        value: values[key],
        onChange: (val) => {
          setValues((prev) => ({
            ...prev,
            // rule out event first, otherwise typescript will complain
            [key]: notEvent<T[K]>(val) ? val : val?.target.value,
          }));
        },
      }),
      [values]
    ),
    /**
     * acts as reset or update from initialValues
     */
    refresh: useCallback(() => {
      setValues(initialValues);
    }, [initialValues]),
  };
};
