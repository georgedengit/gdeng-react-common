import { useCallback, useEffect, useState } from "react";
import { Maybe } from "../../utils/types";

export type UseFormattedInputProps<T> = {
  value: Maybe<T>;
  name: string;
  formatOn?: "onChange" | "onBlur";
  formatter: (val: Maybe<T>) => string;
  parser: (val: string) => Maybe<T>;
  onChange?: (val: Maybe<T>) => void;
  onBlur?: (val: Maybe<T>) => void;
};

export function useFormattedInput<T>({
  value,
  name,
  formatOn = "onBlur",
  formatter,
  parser,
  onChange,
  onBlur,
}: UseFormattedInputProps<T>) {
  const [formattedValue, setFormattedValue] = useState<string>(
    formatter(value)
  );

  const format = useCallback((val: string) => {
    const parsedValue = parser(val);
    setFormattedValue(formatter(parsedValue));
  }, []);

  useEffect(() => {
    onChange && onChange(parser(formattedValue));
  }, [formattedValue]);

  return {
    value: formattedValue,
    name,
    onChange: useCallback<
      React.ChangeEventHandler<HTMLInputElement | HTMLTextAreaElement>
    >((event) => {
      if (formatOn === "onChange") {
        format(event.target.value);
      } else {
        setFormattedValue(event.target.value);
      }
    }, []),
    onBlur: useCallback<
      React.FocusEventHandler<HTMLInputElement | HTMLTextAreaElement>
    >((event) => {
      if (formatOn === "onBlur") {
        format(event.target.value);
      }
    }, []),
  };
}
