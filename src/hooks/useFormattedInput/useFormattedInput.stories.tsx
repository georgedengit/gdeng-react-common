import { Stack, TextField } from "@mui/material";
import * as React from "react";
import { numberFormatter } from "../useFormattedInput/formatters";
import { numberParser } from "../useFormattedInput/parsers";
import { useRecord } from "../useRecord/useRecord";
import { useFormattedInput } from "./useFormattedInput";

enum Gender {
  F = "female",
  M = "male",
  O = "other",
}

type User = {
  firstName: string;
  lastName: string;
  email: string;
  age: number | null;
  gender: Gender | null;
};

export default {
  title: "Hooks",
};

export const FormattedInput = () => {
  const { values, bindEntry } = useRecord<User>({
    firstName: "",
    lastName: "",
    email: "",
    age: null,
    gender: null,
  });

  const ageInput = useFormattedInput({
    ...bindEntry("age"),
    formatter: numberFormatter,
    parser: numberParser,
  });

  return (
    <div>
      <Stack direction="column" gap={2}>
        <TextField label="First Name" {...bindEntry("firstName")} />
        <TextField label="Last Name" {...bindEntry("lastName")} />
        <TextField label="Email" {...bindEntry("email")} />
        <TextField label="Age" {...ageInput} />

        <pre>{JSON.stringify(values, null, 2)}</pre>
      </Stack>
    </div>
  );
};

FormattedInput.storyName = "Formatted Input";
