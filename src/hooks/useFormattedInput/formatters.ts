import type { Maybe } from "../../utils/types";

export const numberFormatter = (val: Maybe<number>) => (val ?? "").toString();
