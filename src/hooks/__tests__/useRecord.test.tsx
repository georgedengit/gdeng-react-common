import { act, renderHook } from "@testing-library/react";

import { useRecord } from "../useRecord";

describe("useRecord tests", () => {
  const example: any = {
    a: undefined,
    b: null,
    c: "",
    d: 0,
    e: false,
  };

  const partialUpdate = {
    a: "asdf",
    b: 1,
  };

  const fullUpdate = {
    a: "bcde",
    b: 2,
    c: "asdf",
    d: 123440,
    e: true,
  };

  it.each`
    initialValues | expected
    ${undefined}  | ${{}}
    ${null}       | ${{}}
    ${example}    | ${example}
  `(
    "should take in initialValues $initialValues and set value to $expected",
    ({ initialValues, expected }) => {
      const { result } = renderHook(() => useRecord(initialValues));
      expect(result.current.values).toEqual(expected);
    }
  );

  it.each`
    type         | updatedValues    | expectedValues
    ${"partial"} | ${partialUpdate} | ${{ ...example, ...partialUpdate }}
    ${"full"}    | ${fullUpdate}    | ${fullUpdate}
  `(
    "should update $type values correctly",
    ({ updatedValues, expectedValues }) => {
      const { result } = renderHook(() => useRecord(example));
      act(() => {
        result.current.update(updatedValues);
      });
      expect(result.current.values).toEqual(expectedValues);
    }
  );

  it("should bind and update values correctly", () => {
    const { result } = renderHook(() => useRecord(example));
    const { name, value, onChange } = result.current.bindEntry("a");
    // bind correctly
    expect(name).toBe("a");
    expect(value).toBe(undefined);
    act(() => {
      onChange("new value");
    });
    expect(result.current.values.a).toBe("new value");
  });
});
